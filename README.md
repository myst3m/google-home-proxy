# google-home-proxy

Proxy to send a voice to Google Home device directly.
It can be used when users want to push messages.

## Installation
If you don't prefer to build by yourself, you can skip this section since the files are in __dist__ directory.

You need 2 files "dist/google-home-proxy.js" and package.json when you just want to use.
Copy the 2 files and run "npm install" on your host.

If you want to modify some files, you can re-build as the following steps.

Copy a build tool 'boot.sh' from (https://github.com/boot-clj/boot-bin/releases/download/latest/boot.sh)
Rename boot.sh to boot if you prefer and put it to some directory on PATH.


```sh
$ git clone https://gitlab.com/myst3m/google-home-proxy
$ cd google-home-proxy
$ npm install
$ boot prod 
```

You can find google-home-proxy.js in dist directory.

## Usage
If you don't build by yourself, you should install npm modules with package.json in __dist__ directory as below, then run.

```sh
$ cd dist
$ npm install
$ node google-home-proxy.js
```

If you build by yourself, npm modules used in google-home-proxy.js have already copied from npm repositories. Therefore, you can run as below.

### Bootstrap without any option
```sh
$ node dist/google-home-proxy.js
INFO [google-home-proxy.app:127] - {:port 9000, :gh-pattern "Google", :gh-lang "ja", :use-proxy-server false, :proxy-server-url "ws://localhost:4000/ws", :log-level :info}               
INFO [google-home-proxy.app:146] - listening on  9000
```

### Bootstrap with a server by websocket channel to receive messages

This usage is used with a websocket server. Please find (https://gitlab.com/myst3m/websocketty)

```sh
$ node dist/google-home-proxy.js -P -c ws://localhost:4000/ws
INFO [google-home-proxy.app:127] - {:port 9000, :gh-pattern "Google", :gh-lang "ja", :use-proxy-server true, :proxy-server-url "ws://localhost:4000/ws", :log-level :info}                                
INFO [google-home-proxy.app:130] - Connecting server  ws://localhost:4000/ws
INFO [google-home-proxy.app:146] - listening on  9000
```

You can find helps with "-h" option to change IP address, a name of Google Home and listen port on this application and so on .

After bootstrapped, you can send a message as follow.

```sh
$ curl -X POST -i http://localhost:9000/notify -d  "text=こんにちは"
```

Also you can send audio MP3 file in "public" directry.
Here, assume this app is listening on 192.168.1.1:9000.
This IP must be open so that google home can download from.

```sh
$ curl -X POST -i http://192.168.1.1:9000/notify -d  "audio=http://192.168.1.1:9000/sound.mp3"
```


Also, can send to websocketty listening on localhost:4000 like this.
Check connection id by accessing http://localhost:4000 and send the port with the id.

```sh
$ curl -X POST -i http://localhost:4000/ -d  "text=こんにちは" -d "connection=40d88e9c"
```

## Notes
If "No device found" appears in log, you may need to wait for a bit to resolve hosts by mDNS.

## License

Copyright © 2018 Tsutomu Miyashita

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.

