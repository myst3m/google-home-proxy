(set-env!
 :source-paths    #{"src/cljs"}
 :resource-paths  #{"resources"}
 :dependencies '[[adzerk/boot-cljs          "2.1.4"      :scope "test"]
                 [adzerk/boot-cljs-repl     "0.3.3"      :scope "test"]
                 [adzerk/boot-reload        "0.6.0"      :scope "test"]
                 [pandeiro/boot-http        "0.8.3"      :scope "test"]
                 [cider/piggieback "0.3.6" :scope "test"]
                 [org.clojure/tools.nrepl   "0.2.13"     :scope "test"]
                 [org.clojure/core.async "0.4.474"]
                 [org.clojure/clojurescript "1.10.339"]
                 [org.clojure/tools.cli "0.3.7"]
                 [org.bodil/cljs-noderepl "0.1.11"]
                 [com.taoensso/timbre "4.10.0"]])

(require
 '[adzerk.boot-cljs      :refer [cljs]]
 '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl cljs-repl-env]]
 '[adzerk.boot-reload    :refer [reload]]
 '[pandeiro.boot-http    :refer [serve]]
 '[clojure.java.io :as io])

(require '[cljs.repl :as repl] '[cljs.repl.node :as node])


(def +version+ "0.4.0")

(deftask production-env []
  (task-options! cljs {:optimizations :simple
                        :source-map false
                        :compiler-options {:npm-deps {:express "4.16.3"
                                                      :multicast-dns "7.0.0"
                                                      :google-tts-api "2.0.2"
                                                      :castv2-client "1.2.0"
                                                      :ws "5.2.0"}
                                           :install-deps true                                        
                                           :target :nodejs}}
                 sift {:include #{#"js/app.js"}
                       :move {#"js/app.js" "google-home-proxy.js"}}
                 target {:dir #{"dist"}})
  identity)

(deftask development-env []
  (task-options! cljs {:optimizations :none
                        :source-map true
                        :compiler-options {:npm-deps {:express "4.16.3"
                                                      :multicast-dns "7.0.0"
                                                      :google-tts-api "2.0.2"
                                                      :castv2-client "1.2.0"
                                                      :ws "5.2.0"}
                                           :install-deps true                                        
                                           :target :nodejs}}
                 sift {:include #{#"js/app.js" #"package.json"}
                       :move {#"js/app.js" "google-home-proxy.js"}})
  identity)

(deftask copy []
  (fn [next-handler]
    (fn [fileset]
      (let [f (io/file "dist/package.json")]
        (io/make-parents f)
        (spit f (slurp (.getName f)))))))

(deftask prod []
  (comp (production-env)
        (cljs)
        (sift)
        (target)
        (copy)))

(deftask dev []
  (comp (development-env)
        (cljs)
        (sift)
        (target)))



;; Notes:

;; To develop on nodejs with CIDER, it is useful to use 'cider-jack-in-clojurescript by C-c M-J, then select type 'node'.
;; It enables to use C-c C-k to reload and so on without a browser.
