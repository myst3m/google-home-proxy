(ns google-home-proxy.app
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [google-home-proxy.notifier :as nf]
            [cljs.nodejs :as nodejs]
            [clojure.tools.cli :refer [parse-opts]] 
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [cljs.core.async :as a :refer [take! <! >! put!]]))  
 

(def express (js/require "express"))
(def body-parser (js/require "body-parser"))
(defonce ws (js/require "ws"))
  
(nodejs/enable-util-print!)


(def default-port 9000)
;; Caution:
;; Even if device has the name 'Living', mDNS name is still "Google-Home-Mini-xxxx.
;; Therefore device name should be the word used in the mDNS name.
;; Here, you can use "Google-Home" or "Google" and any words matched by regexp.

(def default-gh-pattern "Google")
(def default-gh-lang "ja")
(def default-proxy-url "ws://localhost:4000/ws")


(def options
  [["-p" "--port NUMBER" "Listen on this port"
    :default default-port
    :parse-fn #(js/parseInt %)]
   ["-s" "--gh-ipaddr STRING" "IP address of Google Home"]
   ["-n" "--gh-pattern REGEXP" "Device name of Google Home"
    :default default-gh-pattern]
   ["-x" "--gh-lang STRING" "Language"
    :default default-gh-lang]
   ["-P" "--use-proxy-server" "Connect to the proxy server to receive messages via servers"
    :default false]
   ["-c" "--proxy-server-url STRING" "Proxy server URL. (This option is used with -P) "
    :default default-proxy-url]
   ["-v" "--log-level STRING" "Log Level (default. info)"
    :default :info
    :parse-fn keyword]
   ["-h" "--help"]])
   
(defn url-decode [string]
  (some-> string str (js/decodeURIComponent)))

(defn gen-websocket [url]
  (let [data-ch (a/chan)
        heartbeat-ch (a/chan)
        init-ch (a/chan)
        socket (new ws url)]

    (doto socket 
      (.addEventListener  "error" (fn [e]
                                    (put! init-ch {:alive? false})
                                    ;; (a/close! ch)
                                    (log/error "Connection Error")))
      (.on "open" (fn []
                    (log/info "Ready")
                    (put! init-ch
                          {:channels {:data data-ch :heartbeat heartbeat-ch}
                           :socket socket
                           :alive? true})))
      (.on "message" (fn [data]
                       (let [decoded-data (url-decode data)]
                         (put! data-ch {:data decoded-data :alive? true})
                         (log/info "Recieved via Proxy: " decoded-data))))
      (.on "pong" (fn []
                    (put! heartbeat-ch {:alive? true})
                    (log/debug "pong"))))
    init-ch))


(defn connect [server-url & [{:keys [heartbeat-interval retry-interval]
                             :or {heartbeat-interval 3000
                                  retry-interval 1000}}]]
  (let [no-response-count (atom 0)]
    (go-loop [init-ch (gen-websocket server-url)]
      (let [{:keys [channels socket alive?] :as c} (<! init-ch)]
        (if-not alive?
          (do (<! (a/timeout retry-interval))
              (a/close! init-ch)
              (recur (gen-websocket server-url)))
          (let [x (a/alt!
                    (:data channels) ([{:keys [data] :as result}]
                                      (nf/speech (clj->js (-> data (str/replace #"\+" " "))) "ja")
                                      (conj c result))
                    (:heartbeat channels) ([result]
                                           (reset! no-response-count 0)
                                           c)
                    (a/timeout heartbeat-interval) ([result]
                                                    (if (< 2 @no-response-count)
                                                      (conj c {:alive? false})
                                                      (do (.ping socket (fn [_] (swap! no-response-count inc)))
                                                          (log/debug "ping: " @no-response-count)
                                                          c))))]
            (if (:alive? x)
              (do (put! init-ch x)
                  (recur init-ch))
              (do
                (<! (a/timeout retry-interval))
                (.terminate socket)
                (doseq [ch (vals channels)] (a/close! ch))
                (a/close! init-ch)
                (reset! no-response-count 0)
                (recur (gen-websocket server-url))))))))))


(defn -main [& args]
  (let [{:keys [options summary]} (parse-opts args options)
        {:keys [log-level port gh-ipaddr gh-pattern gh-lang proxy-server-url use-proxy-server help]
         :as opts} options]

    (log/set-level! log-level)
    
    (if help
      (doto js/console 
        (.log  "Usage:\n")
        (.log  summary "\n" "\n"))
      (let [app (express)
            parser (-> body-parser (.urlencoded (clj->js {:extended false})))]

        (log/info opts)
        ;; Connect to Proxy if given
        (when use-proxy-server
          (log/info "Connecting server " proxy-server-url)
          (connect proxy-server-url))
        
        (nf/start (cond-> {:pattern gh-pattern}
                    gh-ipaddr (assoc :ip gh-ipaddr)))
        (doto app
          (.use (.static express "public"))
          (.post "/notify" parser (fn [req res]
                                    (let [text (-> req .-body .-text)
                                          audio-url (-> req .-body .-audio)
                                          resource (or audio-url text)]
                                      (log/info "Recieved:" resource)
                                      (if audio-url
                                        (nf/play audio-url)
                                        (nf/speech text gh-lang))
                                      (-> res (.send (str "'" resource "'" "has been delivered\n"))))))
          (.listen port "0.0.0.0" (fn [] (log/info "listening on " port "\n"))))))))

(set! *main-cli-fn* -main)

;; A command to test for a server listening on 9000 directly
;; curl -X POST -i http://localhost:9000/notify -d  "text=こんにちは"

;; A command to test for a server listening on 4000 via Proxy
;; curl -X POST -i http://proxy:4000/ -d  "text=こんにちは" -d "connection=40d88e9c"

;; A command to send audio url provided by this express server
;; Need to leave MP3 file into public directory
;; ex. host is listening on 192.168.1.1:9000 and bounce.mp3 is in public directory
;; curl -X POST -i http://192.168.1.1:9000/notify -d  "audio=http://192.168.1.1:9000/bounce.mp3"

