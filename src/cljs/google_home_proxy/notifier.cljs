(ns google-home-proxy.notifier
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [taoensso.timbre :as log]
            [cljs.core.async :as a :refer [take! <! >! put!]]))


(def cast-v2 (js/require "castv2-client"))
(def CastClient (.-Client cast-v2))
(def default-media-receiver (.-DefaultMediaReceiver cast-v2))
(def mdns (js/require "multicast-dns"))
(def google-tts (js/require "google-tts-api"))

(def devices (atom #{}))


(defn service [record]
  (->> (concat (:answers record) (:additionals record))
       (group-by :type)
       (map vec)
       (map #(update-in % [0] keyword))
       (into {})))

(defn query
  ([]
   (query (mdns)))
  ([mdns-client]
   (.query mdns-client (clj->js {:questions [{:name "_googlecast._tcp.local" :type "PTR"}]}))))

(defn start-listener []
  (doto (mdns)
    (.on "response" (fn [res]
                      (let [record (js->clj res :keywordize-keys true)
                            {:keys [SRV] :as s} (service record)]
                        (log/debug SRV)
                        (when (some #(re-find #"googlecast" (:name %)) SRV)
                          (swap! devices conj (dissoc s :TXT)))))))
  (go-loop []
    (<! (a/timeout 2000))
    (when-not (seq @devices)
      (query)
      (recur))))

(def url-queue (a/chan 10))
(def state (a/chan))

(defn speech [text lang]
  (let [url (.getAudioUrl google-tts text (clj->js {:lang lang
                                                    :host "https://translate.google.com"
                                                    :slow false}))]
      (put! url-queue url)))

(defn play [url]
  (put! url-queue  url))


(defn start-notifier [name-pattern & ip]
  (go-loop []
    (let [url (<! url-queue)
          c (CastClient.)
          devs (->> @devices
                    (filter (fn [{:keys [PTR]}]
                              (some (fn [{:keys [data]}]
                                      (re-find (re-pattern name-pattern) data)) PTR))))
          ip-addrs (filter identity (distinct (concat (map #(-> % :A first :data) devs) ip)))]
      (log/debug "Audio URL: " url)
      (when-not (seq ip-addrs)
        (log/warn (str "No device found matched with " name-pattern " and any IP")))
      (doseq [ip ip-addrs]
        (log/info "Target device IP: " ip)
        (.connect c ip (fn [] (put! state {:state :connected})))
        (<! state)
        (.launch c default-media-receiver (fn [err player]
                                            (put! state {:state :launched
                                                         :player player
                                                         :err err})))
        (let [{:keys [player err]} (<! state)]
          (if err
            (log/error (js->clj err))
            (doto player
              (.on "status" (fn [status]
                              (log/debug (js->clj status))))
              (.load (clj->js {:contentId url
                               :contentType "audio/mp3"
                               :streamType "BUFFERED"})
                     (clj->js {:autoplay true})
                     (fn [err status]
                       (.close c)
                       (log/debug "Device notified"))))))))
    (recur)))

(defn start [{:keys [pattern ip] :or {ip []}}]
  (start-listener)
  (apply start-notifier pattern (flatten [ip])))
